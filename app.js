// setup modules/dependencies

const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js")

// server setup

const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/tasks", taskRoutes);

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.tmdxo2p.mongodb.net/B229_to-do?retryWrites=true&w=majority", {

	useNewUrlParser : true,
	useUnifiedTopology : true

});

// set notification for connection success or failure

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

app.listen(port, () => console.log(`Server running at port ${port}.`))
