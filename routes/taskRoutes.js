const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js")

// [SECTION] - Routes

// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller


router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// creating a new task

router.post("/", (req, res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Delete Task

router.delete("/:id", (req,res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.put("/:id", (req,res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Get specific task

router.get("/:id", (req, res) => {
	taskControllers.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// update status to complete
router.put("/:id/complete", (req,res)=>{
	taskControllers.updateStatus(req.params.id).then(resultFromController => res.send(resultFromController));
})



module.exports = router;