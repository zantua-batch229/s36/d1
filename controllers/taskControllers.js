const Task = require("../models/tasks.js")

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	});
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskID) => {
	return Task.findByIdAndRemove(taskID).then((removedTask, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return removedTask;
		}
	})
} 
module.exports.updateTask = (taskId, newname) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		
		result.name = newname.name;

		return result.save().then((updatedTask, err) => {
			if(error){
				console.log(error);
			}else{
				return updatedTask;
			}


		})
		
	})
}

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;

		}else{
			return result;
		}
	})
}
module.exports.updateStatus = (taskID) => {
	return Task.findById(taskID).then((result, err) => {
		if(err){
			console.log(err);

		}

		result.status = "complete";
		return result.save().then((updatedStatus, err) => {
			if(err){
				console.log(err);
				return false;
			}else{
				return updatedStatus;
			}
		})
	})
}